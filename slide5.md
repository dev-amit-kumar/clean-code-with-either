```
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Apply'
import { pipe } from 'fp-ts/function'
```

### Horse array

```typescript
const horses: Horse[] = [
  {
    type: 'HORSE',
    name: 'CHAMPION',
    legs: 3,
    hasTail: false,
  },
  {
    type: 'HORSE',
    name: 'HOOVES_GALORE',
    legs: 4,
    hasTail: true,
  },
]
```

```typescript
type StandardHorse = {
  name: string
  hasTail: true
  legs: 4
  type: 'STANDARD_HORSE'
}

type TailCheckError =
  | { type: 'HAS_NO_TAIL' }
  | { type: 'TOO_MANY_LEGS' }
  | { type: 'NOT_ENOUGH_LEGS' }
```

- `standardise :: Horse -> Either TailCheckError StandardHorse`

```typescript
const standardise = (
  horse: Horse
): Either<TailCheckError, StandardHorse> => {
  if (!horse.hasTail) {
    return left({ type: 'HAS_NO_TAIL' })
  }
  if (horse.legs < 4) {
    return left({ type: 'NOT_ENOUGH_LEGS' })
  }
  if (horse.legs > 4) {
    return left({ type: 'TOO_MANY_LEGS' })
  }
  return right({
    name: horse.name,
    hasTail: true,
    legs: 4,
    type: 'STANDARD_HORSE',
  })
}
{
    type: 'HORSE',
    name: 'CHAMPION',
    legs: 4,
    hasTail: true,
}

const standardHorse = standardise(horse[1])
if (standardHorse.type === 'Left') {
  return console.log(standardHorse.value) -> { type: 'HAS_NO_TAIL' }
} else {
  return `What a good horse named ${standardHorse.value.name}` ->
}
```
