```
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Apply'
import { pipe } from 'fp-ts/function'
```
## map :: (A -> B) -> Either E A -> Either E B

```typescript 
export const map = <E, A, B>(
  fn: (a: A) => B,
  either: Either<E, A>,
): Either<E, B> => (either.type === 'Left' ? either : right(fn(either.value)));
```

## bind :: (A -> Either E B) -> Either E A -> Either E B

```typescript 
export const bind = <E, A, B>(
  fn: (a: A) => Either<E, B>,
  either: Either<E, A>,
): Either<E, B> => (either.type === 'Left' ? either : fn(either.value));
```

## match :: (E -> B) -> (A -> B) -> Either E A -> B

```typescript 
export const matchEither = <A, B, C, E>(leftFn: (e: E) => B, rightFn: (a:A) => C, either: Either<E, A>) : B| C => (either.type === 'Left' ? leftFn(either.value) : rightFn(either.value));

const showError = (err: HorseError): string => {
  switch (err.type) {
    case 'TOO_MANY_LEGS':
      return 'There are just too many legs';
    case 'NOT_ENOUGH_LEGS':
      return 'There are simply not enough legs';
    case 'HAS_NO_TAIL':
      return 'You can see that it clearly has no tail';
    case 'HORSE_NOT_FOUND':
      return `The horse ${err.name} cannot be found`;
    default:
      return '';
  }
};
```

