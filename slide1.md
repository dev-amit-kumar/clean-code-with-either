### Structure of Either

```
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Apply'
import { pipe } from 'fp-ts/function'
```

```typescript
type Either<E, A> =
  | { type: 'Left'; value: E }
  | { type: 'Right'; value: A }
```

### Defining LEFT

```typescript
const left = <E>(value: E): Either<E, never> => ({
  type: 'Left',
  value,
})
```

### Defining RIGHT

```typescript
const right = <A>(value: A): Either<never, A> => ({
  type: 'Right',
  value,
})
```
