### What is isLeft and isRight

```
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Apply'
import { pipe } from 'fp-ts/function'
```

```typescript
reCaptchaToken = 'some dummy token'

const checkReCaptcha = await validateHuman(reCaptchaToken)

if (isLeft(checkReCaptcha)) {
  console.log("You're a bot")
} else {
  console.log('Human')
}

async function validateHuman(
  reCaptchaToken: string
): Promise<Either<string, boolean>> {
  const response = await axios.post(
    `https://www.google.com/recaptcha/api/siteverify?secret=${config.RECAPTHA_SECRET_KEY}&response=${reCaptchaToken}`
  )
  const isHuman = response.data.success // it will only return true or false
  if (!isHuman) {
    return left('notHuman')
  }
  return right(isHuman)
}
```
