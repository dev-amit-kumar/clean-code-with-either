// Structure of Either

type Either<E, A> =
  | { type: 'Left'; value: E }
  | { type: 'Right'; value: A }

// Defining LEFT

const left = <E>(value: E): Either<E, never> => ({
  type: 'Left',
  value,
})

// Defining RIGHT

const right = <A>(value: A): Either<never, A> => ({
  type: 'Right',
  value,
})
