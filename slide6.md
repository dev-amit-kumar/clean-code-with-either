### We're going to validate this Form to check that everything is good

```
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Apply'
import { pipe } from 'fp-ts/function'
```

```typescript
type Form = {
  name: string
  age: number
  agreesToTnC: boolean
}

type FormError =
  | 'NO_NAME'
  | 'NAME_IS_TESTING'
  | 'AGE_TOO_LOW'
  | 'MUST_AGREE_TnC'
```

### if everything goes well, we'll have this new type that shows our

### form has been validated. Using a new type means we can use the type system

### to track where we're using validated vs non-validated data

```typescript
type GoodForm = {
  type: 'GoodForm'
  name: string
  age: number
  agreesToTnC: true
}
```

### here is our first validator, that checks whether the name is not empty and is not 'testing'

```typescript
const checkName = (
  name: string
): E.Either<FormError, string> => {
  if (name.length < 1) {
    return E.left('NO_NAME')
  }
  if (name.toLowerCase() == 'testing') {
    return E.left('NAME_IS_TESTING')
  }
  return E.right(name)
}

checkName('Bruce') -> E.right('Bruce')
checkName('Testing') -> E.left('NAME_IS_TESTING')
checkName('') -> E.left('NO_NAME')
```

### now we are going to check the age is valid. The only thing we are

### worried about is whether it's non-negative for now

```typescript
const checkAge = (age: number): E.Either<FormError, number> =>
  age > -1 ? E.right(age) : E.left('AGE_TOO_LOW')

checkAge(100) -> E.right(100)
checkAge(-1) -> E.left('AGE_TOO_LOW')
```

### we need to validate whether the user agrees to receive

### marketing communications as we've decided those are not

### optional at all

```typescript
const checkMarketing = (
agreesToTnC: boolean
): E.Either<FormError, true> =>
agreesToTnC
? E.right(true)
: E.left('MUST_AGREE_TnC')

checkMarketing(true) -> E.right(true)
checkMarketing(false) -> E.left('MUST_AGREE_TnC')
```

### make a sequence function - we must tell it to use Either

```typescript
const sequenceT = A.sequenceT(E.either)

export const checkForm = (
form: Form
): E.Either<FormError, GoodForm> =>
pipe(
    // sequenceT takes a tuple of Either and turns them into either
    // a Left with the first error in
    // or a Right containing a tuple of good responses
    sequenceT(
    checkName(form.name),
    checkAge(form.age),
    checkMarketing(form.agreesToTnC)
    ),
    // if they all succeed, we can take all the winning values out
    // and combine them in our new GoodForm
    E.map(([name, age, marketing]) => ({
        type: 'GoodForm',
        name,
        age,
        agreesToTnC: marketing,
    }))
)

checkForm({
    name: 'Bruce',
    age: 100,
    agreesToTnC: true,
})
-> Right({ type: "GoodForm", name: "Bruce", age: 100, agreesToTnC: true})

checkForm({
    name: 'Bruce',
    age: -100,
    agreesToTnC: false,
})
-> Left('AGE_TOO_LOW')
```
