type Either<E, A> =
  | { type: 'Left'; value: E }
  | { type: 'Right'; value: A }

const left = <E>(value: E): Either<E, never> => ({
  type: 'Left',
  value,
})

const right = <A>(value: A): Either<never, A> => ({
  type: 'Right',
  value,
})

const divide = (
  dividend: number,
  divisor: number
): Either<string, number> => {
  if (divisor === 0) {
    return left('Cannot divide by zero')
  }
  return right(dividend / divisor)
}

let result1 = divide(10, 2)
console.log(result1)

let result2 = divide(10, 0)
console.log(result2)