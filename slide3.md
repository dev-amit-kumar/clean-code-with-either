### Simple Example

```typescript
const divide = (dividend: number,divisor: number)=> {
    if (divisor === 0) {
        return('Cannot divide by zero')
    }
    return (dividend / divisor)
}

divide(10, 2) -> 5
divide(100, 0) -> "Cannot divide by zero"

```

### Either example

```typescript
const divide = (dividend: number,divisor: number): Either<string, number> => {
    if (divisor === 0) {
        return left('Cannot divide by zero')
    }
    return right(dividend / divisor)
}

let result1 = divide(10, 2) -> { type: "Right", value: 5 }
let result2 = divide(100, 0) -> { type: "Left", value: "Cannot divide by zero" }

if(result1.type === 'Right'){
    //success code
}else{
    // failure code
}

```
