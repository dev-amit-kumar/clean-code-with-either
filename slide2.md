### Simple Example

```
import * as E from 'fp-ts/Either'
import * as A from 'fp-ts/Apply'
import { pipe } from 'fp-ts/function'
```

```typescript
const divide = (dividend: number,divisor: number)=> {
    if (divisor === 0) {
        return('Cannot divide by zero')
    }
    return (dividend / divisor)
}

divide(10, 2) -> 5
divide(100, 0) -> "Cannot divide by zero"

```
